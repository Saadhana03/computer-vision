#include <iostream>
#include <fstream>
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <math.h>
//#include <conio.h>
#include <Eigen/Dense>
#include <OpenNI.h>
#include <PS1080.h>
/**
 * @brief Saving point clouds data as ply file
 * @param [in] filename : filename for saving point clouds (the extention should be .ply)
 * @param [in] vertices : list of vertex point
 * @param [in] colors   : list of vertex color
 * @return Success:true, Failure:false
 */
bool savePointCloudsPLY(std::string filename, std::vector<Eigen::Vector4f>& vertices, std::vector<cv::Vec3b>& colors) {
    std::ofstream fout;
    fout.open(filename.c_str());
    if (fout.fail()) {
        std::cerr << "file open error:" << filename << std::endl;
        return false;
    }
    int pointNum = vertices.size();
    fout << "ply" << std::endl;
    fout << "format ascii 1.0" << std::endl;
    fout << "element vertex " << pointNum << std::endl;
    fout << "property float x" << std::endl;
    fout << "property float y" << std::endl;
    fout << "property float z" << std::endl;
    fout << "property uchar red" << std::endl;
    fout << "property uchar green" << std::endl;
    fout << "property uchar blue" << std::endl;
    fout << "property uchar alpha" << std::endl;
    fout << "end_header" << std::endl;
    for (int i = 0; i < pointNum; i++) {
        Eigen::Vector4f& vertex = vertices[i];
        cv::Vec3b& col = colors[i];
        fout << vertex[0] << " " << vertex[1] << " " << vertex[2] << " " << static_cast<int> (col[2]) << " " << static_cast<int> (col[1]) << " " << static_cast<int> (col[0]) << " " << 255 << std::endl;
    }
    fout.close();
    return true;
}
// Convert to colored depth image
cv::Mat convColoredDepth(cv::Mat& depthImg, float minThresh = 0, float maxThresh = 0) {
    cv::Mat coloredDepth = depthImg.clone();
    double min;
    double max;
    if (minThresh == 0 && maxThresh == 0) {
        cv::minMaxIdx(depthImg, &min, &max);
    } else {
        min = minThresh;
        max = maxThresh;
    }
    coloredDepth -= min;
    cv::convertScaleAbs(coloredDepth, coloredDepth, 255 / (max - min));
    cv::applyColorMap(coloredDepth, coloredDepth, cv::COLORMAP_JET);
    return coloredDepth;
}
int main(int argc, char* argv[]) {
    ////
    // Initialize OpenNI video capture
    if (openni::OpenNI::initialize() != openni::STATUS_OK) {
        std::cout << "Error Initializing OpenNi" << std::endl;
    }
    //    openni::Array<openni::DeviceInfo> devices;
    //    openni::OpenNI::enumerateDevices(&devices);
    //    
    //    std::cout<<devices[0].getUri()<<"  "<<devices[0].getName();
    ////
    openni::Device connDevice;
    if (connDevice.open(openni::ANY_DEVICE) != openni::STATUS_OK) {
        std::cout << "Error opening Device";
    }
    //    
    //    // Create depth camera stream
    //    
    openni::VideoStream depthStream;
    if (depthStream.create(connDevice, openni::SENSOR_DEPTH) != openni::STATUS_OK) {
        std::cout << "Error creating depth stream";
        return -1;
    }
    //    
    //    // Create color camera stream
    //    
    openni::VideoStream colorStream;
    if (colorStream.create(connDevice, openni::SENSOR_COLOR) != openni::STATUS_OK) {
        std::cout << "Error creating color stream";
        return -1;
    }
    //    ////
    //    // Set flag for synchronization between color camera and depth camera
    connDevice.setDepthColorSyncEnabled(true);
    //    
    //    // Set flag for registration between color camera and depth camera
    connDevice.setImageRegistrationMode(openni::ImageRegistrationMode::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
    //    
    //    ////
    //    // Get focal length of IR camera in mm for VGA resolution
    int zero_plan_distance;
    if (depthStream.getProperty(XN_STREAM_PROPERTY_ZERO_PLANE_DISTANCE, &zero_plan_distance) != openni::STATUS_OK) {
        std::cout << "Error getting distance";
    }
    //   
    //    
    //    // Convert focal length from mm -> pixels (valid for 640x480)
    double pixel_size = 0;
    if (depthStream.getProperty<double>(XN_STREAM_PROPERTY_ZERO_PLANE_PIXEL_SIZE, &pixel_size) != openni::STATUS_OK) {
        std::cout << "Error reading pixel size";
    }
    std::cout << "Size: " << pixel_size;
    pixel_size = pixel_size * 2.0;
    double focal_length = (int) ((double) zero_plan_distance / pixel_size);
    std::cout << "focal length   " << focal_length << std::endl;
    double px = 319.5;
    double py = 239.5;
    float minRange = 0;
    float maxRange = 4000;
    bool std_in_progress = false;
    //Eigen::MatrixXf stdDevMat;
    cv::Mat std_images[100];
    int count = 0;
    // Start loop
    while (true) {
        ////
        // Grab depth image
        openni::VideoFrameRef depthFrameRef;
        depthStream.start();
        cv::Mat depthImage;
        if (depthStream.readFrame(&depthFrameRef) == openni::STATUS_OK) {
            depthImage = cv::Mat(depthFrameRef.getHeight(), depthFrameRef.getWidth(), CV_16UC1, (void*) depthFrameRef.getData());
        }
        // Grab color image
        openni::VideoFrameRef colorFrameRef;
        colorStream.start();
        cv::Mat colorImage;
        if (colorStream.readFrame(&colorFrameRef) == openni::STATUS_OK) {
            colorImage = cv::Mat(colorFrameRef.getHeight(), colorFrameRef.getWidth(), CV_8UC3, (void*) colorFrameRef.getData());
            cv::cvtColor(colorImage, colorImage, cv::COLOR_BGR2RGB);
        }
        // Show images
        cv::imshow("colorImage", colorImage);
        cv::imshow("depthImage", convColoredDepth(depthImage, minRange, maxRange));
        int key = cv::waitKey(10);
        //std::cout<<"key "<<(char)key<<"  ";
        // Convert the depth iamge to point clouds and save it.
        if ((char) key == 's') {
            std::vector<Eigen::Vector4f> vertices; // 3D points
            std::vector<cv::Vec3b> colors; // color of the points
            for (int i = 0; i < colorImage.size().width; i++) {
                for (int j = 0; j < colorImage.size().height; j++) {
                    Eigen::Vector4f vertex;
                    float z = (depthImage.at<unsigned short>(j, i));
                    vertex(2) = z;
                    vertex(0) = (i - px) * z / focal_length;
                    vertex(1) = (j - py) * z / focal_length;
                    vertex(3) = 1;
                    vertices.push_back(vertex);
                    colors.push_back(colorImage.at<cv::Vec3b>(j, i));
                }
            }
            savePointCloudsPLY("s_pointClouds.ply", vertices, colors);
        }
        ////
        // Compute standard deviation of depth stream and save it
        if ((char) key == 'd' || std_in_progress == true) {
            std_in_progress = true;
            //std::cout << "IN D   " << count << std::endl;
            if (count < 100) {
                //Continue getting frames
                std_images[count++] = depthImage;
            } else if (count > 100) {
                count = 0;
                std_in_progress = false;
            } else {
                count = 0;
                std_in_progress = false;
                cv::Mat mat_mean = cv::Mat(depthImage.rows, depthImage.cols, CV_32FC1, cv::Scalar(0));
                //mat_mean.Zero(depthImage.rows, depthImage.cols);
                //std::cout << "size of mat:  " << mat_mean.rows;
                //                cv::Mat covar;
                //                cv::calcCovarMatrix(std_images, 100, covar, mat_mean, CV_COVAR_NORMAL, CV_16UC1);
                for (int i = 0; i < 100; i++) {
                    std_images[i].convertTo(std_images[i], CV_32FC1);
                    cv::accumulate(std_images[i], mat_mean);
                    //                    for (int j = 0; j < depthImage.rows; j++) {
                    //                        for (int k = 0; k < depthImage.cols; k++) {
                    //                            mat_mean.at<unsigned short>(j, k) += (std_images[i].at<unsigned short>(j, k))*0.01;
                    //                        }
                    //                    }
                }
                mat_mean *= 0.01;
                cv::imshow("mean", convColoredDepth(mat_mean, minRange, maxRange));
                cv::waitKey(1000);
                cv::imwrite("mean_image.png", convColoredDepth(mat_mean, minRange, maxRange));
                cv::Mat mat_std = cv::Mat(depthImage.rows, depthImage.cols, CV_32FC1, cv::Scalar(0));
                cv::Mat tem_mat;
                for (int i = 0; i < 100; i++) {
                    cv::pow(std_images[i] - mat_mean, 2, tem_mat);
                    cv::accumulate(tem_mat, mat_std);
                }
                //mat_std.convertTo(mat_std, CV_32FC1);
                mat_std *= 0.01;
                cv::sqrt(mat_std, mat_std);
                
                cv::imshow("std image", convColoredDepth(mat_std, minRange, maxRange));
                cv::waitKey(1000);
                cv::imwrite("stddev_image.png", convColoredDepth(mat_std, minRange, maxRange));
//                mat_std.convertTo(mat_std, CV_16UC1);
//                mat_mean.convertTo(mat_mean, CV_16UC1);
                std::vector<Eigen::Vector4f> vertices; // 3D points
                std::vector<cv::Vec3b> colors; // color of the points
                //mat_std = convColoredDepth(mat_std, minRange, maxRange);
                for (int i = 0; i < mat_mean.size().width; i++) {
                    for (int j = 0; j < mat_mean.size().height; j++) {
                        Eigen::Vector4f vertex;
                        float z = (mat_mean.at<float>(j, i));
                        vertex(2) = z;
                        vertex(0) = (i - px) * z / focal_length;
                        vertex(1) = (j - py) * z / focal_length;
                        vertex(3) = 1;
                        vertices.push_back(vertex);
                        //float a = mat_std.at<unsigned short>(i, j);
                        //cv::Vec3b color(a, 0, 0);
                        colors.push_back(convColoredDepth(mat_std, minRange, maxRange).at<float>(j, i));
                    }
                }
                savePointCloudsPLY("d_pointClouds.ply", vertices, colors);
                //mat_std/=100;
                //                //Eigen::sqrt(mat_stddev);
                //                
                //                
                //                cv::Mat meanImage;
                //                cv::eigen2cv(&mat_mean, &meanImage);
            }
        }
        ////
        // Compute mesh of point clouds and save it
        if (key == 'm') {
        }
        if (key == 'q' || key == 27) {
            break;
        }
    }
    ////
    // destroy image streams and close the OpenNI device
    depthStream.destroy();
    colorStream.destroy();
    connDevice.close();
    openni::OpenNI::shutdown();
    return 0;
}
