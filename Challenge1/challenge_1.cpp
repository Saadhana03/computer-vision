#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/correspondence.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/board.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/recognition/cg/hough_3d.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <pcl/recognition/hv/hv_go.h>
#include <pcl/registration/icp.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/recognition/hv/greedy_verification.h>
#include <pcl/io/ply_io.h>
#include <pcl-1.8/pcl/visualization/cloud_viewer.h>
#include<pcl/apps/render_views_tesselated_sphere.h>
#include <pcl/io/vtk_lib_io.h>
#include <vtkPolyDataMapper.h>
#include <opencv2/imgproc.hpp>
#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/search/flann_search.h>
#include <pcl/sample_consensus/mlesac.h>
#include <pcl/sample_consensus/rmsac.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/sample_consensus/sac_model_sphere.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/vfh.h>
#include <boost/filesystem.hpp>
#include <boost/lambda/bind.hpp>
#include <exception>
#include <fstream>
#include <ctime>
#include <stdio.h>
#include <vector>
#include <string>
#include <sstream>
#include <stdlib.h>
typedef pcl::PointXYZRGBA PointType;
typedef pcl::Normal NormalType;
typedef pcl::ReferenceFrame RFType;
typedef pcl::SHOT352 DescriptorType;
std::string projectSrcDir = PROJECT_SOURCE_DIR;
static const int KEYPOINT_GENERATOR = 1;
static const int KEYPOINT_SIFT = 1;
static const int KEYPOINT_VOXELIZATION = 2;
static const int KEYPOINT_UNIFORM_SAMPLING = 3;
static const bool KEYPOINT_MODEL = true;
static const bool KEYPOINT_SCENE = false;
static const float downsampling_rad_model = 0.006;
static const float downsampling_rad_scene = 0.004;
static const float keypoint_voxelization_rad = 0.01;
static const float uniform_sam_rad = 0.02;
static const float normal_est_rad = 0.04;
static const float shot_support_rad = 0.02;
static const float cg_size = 0.02;
static const float cg_thres = 5;
static const float greedy_hv_resolution = 0.005f;
static const float greedy_hv_inlierthreshold = 0.005f;
static const float greedy_hv_lambda = 1.5f;
static const float ransac_rad_low = 0.05;
static const float ransac_rad_high = 0.1;
static const int NUM_CLASSES = 5;
static const int BIRD = 4;
static const int CAN = 5;
static const int CRACKER = 1;
static const int HOUSE = 3;
static const int SHOE = 2;
static const int NUM_TRAIN_IMAGES = 2561;
static const int TRAIN_INC = 50;
static const std::string TEAMNAME = "TEAM6";
using namespace boost::filesystem;
using namespace boost::lambda;
void visualize(pcl::PointCloud<PointType>::Ptr cloud, std::string desc) {
    pcl::visualization::CloudViewer cloudViewer(desc);
    cloudViewer.showCloud(cloud);
    while (!cloudViewer.wasStopped()) {
    }
}
void visualize_normals(pcl::PointCloud<PointType>::Ptr cloud, pcl::PointCloud<NormalType>::Ptr normals, std::string desc) {
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer(desc));
    viewer->setBackgroundColor(0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBAField<PointType> rgb(cloud);
    viewer->addPointCloud<PointType>(cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    viewer->addPointCloudNormals<PointType, NormalType>(cloud, normals, 10, 0.1, "normals");
    while (!viewer->wasStopped()) {
        viewer->spinOnce(100);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
}
void visualize_corres(pcl::PointCloud<PointType>::Ptr model, pcl::PointCloud<PointType>::Ptr scene, pcl::Correspondences corres, std::string desc) {
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer(desc));
    viewer->setBackgroundColor(0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBAField<PointType> rgb(model);
    Eigen::Matrix4f t;
    t << 1, 0, 0, 2,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1;
    pcl::transformPointCloud(*model, *model, t);
    viewer->addPointCloud<PointType>(model, rgb, "sample cloud");
    viewer->addPointCloud<PointType>(scene, rgb, "sample clou");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "sample clou");
    viewer->addCorrespondences<PointType>(model, scene, corres, 1);
    //viewer->addCorrespondences(
    while (!viewer->wasStopped()) {
        viewer->spinOnce(100);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
}
void visualize_model_scene(pcl::PointCloud<PointType>::Ptr model, pcl::PointCloud<PointType>::Ptr scene, std::string desc) {
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_n(new pcl::visualization::PCLVisualizer("model_scene"));
    viewer_n->setBackgroundColor(0, 0, 0);
    viewer_n->addPointCloud<PointType>(model, "model");
    viewer_n->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, "model");
    viewer_n->addPointCloud<PointType>(scene, "scene");
    viewer_n->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "scene");
    viewer_n->initCameraParameters();
    //while (!viewer_n->wasStopped()) {
    viewer_n->spinOnce(100);
    boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    //}
}
void visualize_with_keypoints(pcl::PointCloud<PointType>::Ptr cloud, pcl::PointCloud<PointType>::Ptr keypoints, std::string desc) {
    pcl::visualization::PCLVisualizer viewer(desc);
    pcl::visualization::PointCloudColorHandlerCustom<PointType> keypoints_color_handler(keypoints, 0, 255, 0);
    pcl::visualization::PointCloudColorHandlerCustom<PointType> cloud_color_handler(cloud, 255, 255, 0);
    viewer.setBackgroundColor(0.0, 0.0, 0.0);
    viewer.addPointCloud(cloud, "ModelCloud");
    viewer.addPointCloud(keypoints, keypoints_color_handler, "keypointModel");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "keypointModel");
    while (!viewer.wasStopped()) {
        viewer.spinOnce();
    }
}
void tesselation_stuff_not_done() {
    const char* arg = "challenge_train/models/house.ply";
    vtkSmartPointer<vtkPLYReader> vtkReader = vtkSmartPointer<vtkPLYReader>::New();
    vtkReader->SetFileName(arg);
    vtkReader->Update();
    vtkSmartPointer<vtkPolyDataMapper> polyDataMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    polyDataMapper->SetInputConnection(vtkReader->GetOutputPort());
    polyDataMapper->Update();
    vtkSmartPointer<vtkPolyData> vtkObject = polyDataMapper->GetInput();
    //std::vector<pcl::PointCloud<>>
    pcl::apps::RenderViewsTesselatedSphere rendered_views;
    rendered_views.setResolution(640);
    //rendered_views.setViewAngle()
    rendered_views.setTesselationLevel(1);
    rendered_views.addModelFromPolyData(vtkObject);
    rendered_views.generateViews();
    std::vector<pcl::PointCloud< pcl::PointXYZ>::Ptr> views;
    std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > poses;
    rendered_views.getPoses(poses);
    rendered_views.getViews(views);
    //std::cout << "Number of Views: " << views.size();
}
bool savePointCloudsPLY(std::string filename, std::vector<Eigen::Vector4f>& vertices, std::vector<cv::Vec3b>& colors) {
    std::ofstream fout;
    fout.open(filename.c_str());
    if (fout.fail()) {
        std::cerr << "file open error:" << filename << std::endl;
        return false;
    }
    int pointNum = vertices.size();
    fout << "ply" << std::endl;
    fout << "format ascii 1.0" << std::endl;
    fout << "element vertex " << pointNum << std::endl;
    fout << "property float x" << std::endl;
    fout << "property float y" << std::endl;
    fout << "property float z" << std::endl;
    fout << "property uchar red" << std::endl;
    fout << "property uchar green" << std::endl;
    fout << "property uchar blue" << std::endl;
    fout << "property uchar alpha" << std::endl;
    fout << "end_header" << std::endl;
    for (int i = 0; i < pointNum; i++) {
        Eigen::Vector4f& vertex = vertices[i];
        cv::Vec3b& col = colors[i];
        fout << vertex[0] << " " << vertex[1] << " " << vertex[2] << " " << static_cast<int> (col[2]) << " " << static_cast<int> (col[1]) << " " << static_cast<int> (col[0]) << " " << 255 << std::endl;
    }
    fout.close();
    return true;
}
void generate_point_clouds_model(std::string color_filename, std::string depth_filename, std::string filename) {
    std::vector<Eigen::Vector4f> vertices;
    std::vector<cv::Vec3b> colors;
    cv::Mat depth_img = cv::imread(depth_filename, CV_LOAD_IMAGE_UNCHANGED);
    cv::Mat color_img = cv::imread(color_filename, CV_LOAD_IMAGE_UNCHANGED);
    //    cv::imshow("depth", depth_img);
    //    cv::imshow("color", color_img);
    float focal = 570.342f; // focal length
    float px = 320.0f; // principal point x
    float py = 240.0f;
    for (int i = 0; i < color_img.size().width; i++) {
        for (int j = 0; j < color_img.size().height; j++) {
            Eigen::Vector4f vertex;
            float z = depth_img.at<unsigned short>(j, i) / 1000.0f;
            vertex(2) = z;
            vertex(0) = ((i - px) * z) / focal;
            vertex(1) = ((j - py) * z) / focal;
            vertex(3) = 1;
            vertices.push_back(vertex);
            colors.push_back(color_img.at<cv::Vec3b>(j, i));
        }
    }
    savePointCloudsPLY(filename, vertices, colors);
}
void generate_point_clouds_scene(std::string color_filename, std::string depth_filename, std::string filename) {
    std::vector<Eigen::Vector4f> vertices;
    std::vector<cv::Vec3b> colors;
    cv::Mat depth_img = cv::imread(depth_filename, CV_LOAD_IMAGE_UNCHANGED);
    cv::Mat color_img = cv::imread(color_filename, CV_LOAD_IMAGE_UNCHANGED);
    //    cv::imshow("depth", depth_img);
    //    cv::imshow("color", color_img);
    float focal = 579.4709; // focal length
    float px = 322.2294; // principal point x
    float py = 244.5676;
    for (int i = 0; i < color_img.size().width; i++) {
        for (int j = 0; j < color_img.size().height; j++) {
            Eigen::Vector4f vertex;
            float z = depth_img.at<unsigned short>(j, i) / 1000.0f;
            vertex(2) = z;
            vertex(0) = ((i - px) * z) / focal;
            vertex(1) = ((j - py) * z) / focal;
            vertex(3) = 1;
            vertices.push_back(vertex);
            colors.push_back(color_img.at<cv::Vec3b>(j, i));
        }
    }
    savePointCloudsPLY(filename, vertices, colors);
}
pcl::PointCloud<PointType>::Ptr remove_planes(pcl::PointCloud<PointType>::Ptr cloud) {
    pcl::PointIndices::Ptr inlierIndices(new pcl::PointIndices);
    pcl::PointCloud<PointType>::Ptr inlierPoints(new pcl::PointCloud<PointType>);
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::SACSegmentation<PointType> segmentation;
    pcl::ExtractIndices<PointType> extract;
    segmentation.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
    segmentation.setMethodType(pcl::SAC_RANSAC);
    segmentation.setDistanceThreshold(0.01);
    segmentation.setOptimizeCoefficients(true);
    long num_points_cloud = (long) cloud->points.size();
    while (cloud->points.size() > 0.2 * num_points_cloud) {
        segmentation.setInputCloud(cloud);
        segmentation.segment(*inlierIndices, *coefficients);
        if (inlierIndices->indices.size() == 0) {
            //std::cout << "No more planes." << std::endl;
            break;
        } else {
            // Copy all inliers of the model to another cloud.
            //std::cout << inlierIndices.get()->indices.size();
            extract.setInputCloud(cloud);
            extract.setIndices(inlierIndices);
            extract.setNegative(true);
            extract.filter(*inlierPoints);
            //visualize(inlierPoints, "inliers");
            cloud.swap(inlierPoints);
        }
    }
    return cloud;
}
pcl::PointCloud<PointType>::Ptr generate_keypoints(pcl::PointCloud<PointType>::Ptr cloud, int technique = KEYPOINT_SIFT, bool model = true) {
    switch (technique) {
            //SIFT
        case KEYPOINT_SIFT:
        {
            if (model == KEYPOINT_MODEL) {
                //std::cout << "getting SIFT keypoints" << std::endl;
                const float min_scale = 0.0005f;
                const int n_octaves = 5; //4
                const int n_scales_per_octave = 10; // 5
                const float min_contrast = 1.0f;
                pcl::SIFTKeypoint<PointType, pcl::PointWithScale> sift;
                pcl::PointCloud<pcl::PointWithScale> result;
                pcl::search::KdTree<PointType>::Ptr tree2(new pcl::search::KdTree<PointType>());
                sift.setSearchMethod(tree2);
                sift.setScales(min_scale, n_octaves, n_scales_per_octave);
                sift.setMinimumContrast(min_contrast);
                sift.setInputCloud(cloud);
                sift.compute(result);
                pcl::PointCloud<PointType>::Ptr sampledModelCloud(new pcl::PointCloud<PointType>);
                copyPointCloud(result, *sampledModelCloud);
                //visualize_with_keypoints(cloud, sampledModelCloud, "sift");
                return sampledModelCloud;
            } else {
                const float min_scale_s = 0.001f; //0.0005
                const int n_octaves_s = 8; //4
                const int n_scales_per_octave_s = 10; // 5
                const float min_contrast_s = 0.3f; // 1.0
                pcl::SIFTKeypoint<PointType, pcl::PointWithScale> sift2;
                pcl::PointCloud<pcl::PointWithScale> result2;
                pcl::search::KdTree<PointType>::Ptr tree3(new pcl::search::KdTree<PointType>());
                sift2.setSearchMethod(tree3);
                sift2.setScales(min_scale_s, n_octaves_s, n_scales_per_octave_s);
                sift2.setMinimumContrast(min_contrast_s);
                sift2.setInputCloud(cloud);
                sift2.compute(result2);
                pcl::PointCloud<PointType>::Ptr sampledSceneCloud(new pcl::PointCloud<PointType>);
                copyPointCloud(result2, *sampledSceneCloud);
                //visualize_with_keypoints(cloud, sampledSceneCloud, "sift_scene");
                return sampledSceneCloud;
            }
        }
        case KEYPOINT_VOXELIZATION:
        {
            //std::cout << "Getting keypoints by Voxelization" << std::endl;
            pcl::VoxelGrid< PointType> voxel_grid;
            pcl::PointCloud<PointType>::Ptr sampledModelCloud(new pcl::PointCloud<PointType>);
            voxel_grid.setInputCloud(cloud);
            voxel_grid.setLeafSize(keypoint_voxelization_rad, keypoint_voxelization_rad, keypoint_voxelization_rad);
            voxel_grid.filter(*sampledModelCloud);
            //visualize_with_keypoints(cloud, sampledModelCloud, "voxelized cloud");
            return sampledModelCloud;
        }
        case KEYPOINT_UNIFORM_SAMPLING:
        {
            std::cout << "Getting keypoints by uniform sampling" << std::endl;
            pcl::UniformSampling<PointType> uniform_sampling;
            uniform_sampling.setRadiusSearch(uniform_sam_rad);
            pcl::PointCloud<PointType>::Ptr sampledModelCloud(new pcl::PointCloud<PointType>);
            uniform_sampling.setInputCloud(cloud);
            uniform_sampling.filter(*sampledModelCloud);
            //visualize_with_keypoints(cloud, sampledModelCloud, "Uniform cloud");
            return sampledModelCloud;
        }
            return NULL;
    }
}
pcl::PointCloud<NormalType>::Ptr get_normals(pcl::PointCloud<PointType>::Ptr cloud) {
    //std::cout << "Calculating normals" << std::endl;
    pcl::PointCloud<NormalType>::Ptr normalCloud(new pcl::PointCloud<NormalType>());
    pcl::search::KdTree<PointType>::Ptr searchTree(new pcl::search::KdTree<PointType> ());
    pcl::NormalEstimationOMP<PointType, NormalType> normalEstimation;
    normalEstimation.setNumberOfThreads(4);
    normalEstimation.setInputCloud(cloud);
    normalEstimation.setSearchMethod(searchTree);
    normalEstimation.setRadiusSearch(normal_est_rad);
    normalEstimation.compute(*normalCloud);
    //visualize_normals(cloud, normalCloud, "Normals");
    return normalCloud;
}
pcl::PointCloud<PointType>::Ptr downsample_cloud(pcl::PointCloud<PointType>::Ptr cloud, float radius, bool model) {
    //std::cout << "Downsampling cloud" << std::endl;
    pcl::VoxelGrid< PointType> voxel_grid;
    pcl::PointCloud<PointType>::Ptr cloud_downsampled(new pcl::PointCloud<PointType>);
    voxel_grid.setInputCloud(cloud);
    if (model == KEYPOINT_MODEL) {
        voxel_grid.setLeafSize(downsampling_rad_model, downsampling_rad_model, downsampling_rad_model);
    } else {
        voxel_grid.setLeafSize(downsampling_rad_scene, downsampling_rad_scene, downsampling_rad_scene);
    }
    voxel_grid.filter(*cloud_downsampled);
    //visualize(cloud_downsampled, "downsampled");
    return cloud_downsampled;
}
std::vector<pcl::PointCloud<PointType>::ConstPtr> run_icp(pcl::PointCloud<PointType>::Ptr model, pcl::PointCloud<PointType>::Ptr scene,
        std::vector< Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f >> transformations) {
    int num_clusters = transformations.size();
    //std::cout << "Refining pose using icp: " << num_clusters;
    std::vector<pcl::PointCloud<PointType>::ConstPtr> registered_model;
    pcl::PointCloud<PointType>::Ptr transformed_model(new pcl::PointCloud<PointType>());
    pcl::PointCloud<PointType>::Ptr cloud_target = scene;
    for (int i = 0; i < num_clusters; i++) {
        //std::cout << "Running icp on " << i << std::endl;
        pcl::transformPointCloud(*model, *transformed_model, transformations[i]);
        pcl::IterativeClosestPoint<PointType, PointType> icp;
        icp.setMaximumIterations(icp.getMaximumIterations()*3);
        icp.setUseReciprocalCorrespondences(true);
        icp.setInputSource(transformed_model);
        icp.setInputTarget(cloud_target);
        pcl::PointCloud<PointType>::Ptr registered(new pcl::PointCloud<PointType>);
        icp.align(*registered);
        registered_model.push_back(registered);
    }
    return registered_model;
}
std::vector<bool> greedy_verify_hypothesis(std::vector<pcl::PointCloud<PointType>::ConstPtr> registered_models,
        pcl::PointCloud<PointType>::Ptr scene) {
    pcl::GreedyVerification<PointType, PointType> greedy_hv(greedy_hv_lambda);
    greedy_hv.setResolution(greedy_hv_resolution); //voxel grid is applied beforehand
    greedy_hv.setInlierThreshold(greedy_hv_inlierthreshold);
    greedy_hv.setSceneCloud(scene);
    greedy_hv.addModels(registered_models, true);
    greedy_hv.verify();
    std::vector<bool> mask_hv;
    greedy_hv.getMask(mask_hv);
    return mask_hv;
}
pcl::PointCloud<PointType>::Ptr eliminate_outliers_ransac(pcl::PointCloud<PointType>::Ptr cloud) {
    pcl::PointCloud<PointType>::Ptr ransac_inliers(new pcl::PointCloud<PointType>);
    std::vector<int> inliers;
    pcl::SampleConsensusModelSphere<PointType>::Ptr sample_consensus_sphere(new pcl::SampleConsensusModelSphere<PointType>(cloud));
    sample_consensus_sphere->setRadiusLimits(ransac_rad_low, ransac_rad_high);
    pcl::RandomSampleConsensus<PointType> ransac(sample_consensus_sphere);
    ransac.setMaxIterations(50000);
    ransac.setDistanceThreshold(.008); //0.01
    ransac.computeModel();
    ransac.getInliers(inliers);
    pcl::copyPointCloud<PointType>(*cloud, inliers, *ransac_inliers);
    //visualize_with_keypoints(cloud, ransac_inliers, "ransac_inliers");
    return ransac_inliers;
}
pcl::PointCloud<pcl::VFHSignature308>::Ptr get_descriptor(pcl::PointCloud<PointType>::Ptr cloud_cluster) {
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<pcl::VFHSignature308>::Ptr descriptor(new pcl::PointCloud<pcl::VFHSignature308>);
    // Estimate the normals.
    pcl::NormalEstimation<PointType, pcl::Normal> normalEstimation;
    normalEstimation.setInputCloud(cloud_cluster);
    normalEstimation.setRadiusSearch(0.03);
    pcl::search::KdTree<PointType>::Ptr kdtree(new pcl::search::KdTree<PointType>);
    normalEstimation.setSearchMethod(kdtree);
    normalEstimation.compute(*normals);
    pcl::VFHEstimation<PointType, pcl::Normal, pcl::VFHSignature308> vfh;
    vfh.setInputCloud(cloud_cluster);
    vfh.setInputNormals(normals);
    vfh.setSearchMethod(kdtree);
    // Optionally, we can normalize the bins of the resulting histogram,
    // using the total number of points.
    vfh.setNormalizeBins(true);
    // Also, we can normalize the SDC with the maximum size found between
    // the centroid and any of the cluster's points.
    vfh.setNormalizeDistance(false);
    vfh.compute(*descriptor);
    return descriptor;
}
pcl::PointCloud<PointType>::Ptr euclidean_clustering(pcl::PointCloud<PointType>::Ptr model, pcl::PointCloud<PointType>::Ptr scene) {
    pcl::PointCloud<pcl::VFHSignature308>::Ptr model_descriptor = get_descriptor(model);
    pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
    tree->setInputCloud(scene);
    std::vector<pcl::PointIndices> cluster_indices;
    std::vector<pcl::PointIndices> model_indices;
    pcl::EuclideanClusterExtraction<PointType> ec;
    ec.setClusterTolerance(0.02); // 2cm
    ec.setMinClusterSize(100);
    ec.setMaxClusterSize(25000);
    ec.setSearchMethod(tree);
    ec.setInputCloud(scene);
    ec.extract(cluster_indices);
    ec.setInputCloud(model);
    ec.extract(model_indices);
    pcl::PointCloud<PointType>::Ptr best_cluster(new pcl::PointCloud<PointType>);
    float max_dist = 9999999999999.0;
    int j = 0;
    Eigen::Vector4f centroid_scene;
    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {
        pcl::PointCloud<PointType>::Ptr cloud_cluster(new pcl::PointCloud<PointType>);
        for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
            cloud_cluster->points.push_back(scene->points[*pit]); //*
        cloud_cluster->width = cloud_cluster->points.size();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;
        //Descriptor
        pcl::PointCloud<pcl::VFHSignature308>::Ptr scene_descriptor = get_descriptor(cloud_cluster);
        //pcl::io::savePCDFileASCII("test_pcd.pcd", *scene_descriptor);
        //Find correspondences
        pcl::KdTreeFLANN<pcl::VFHSignature308>::Ptr kdtree_flann(new pcl::KdTreeFLANN<pcl::VFHSignature308>);
        model_descriptor->is_dense = true;
        scene_descriptor->is_dense = true;
        kdtree_flann->setInputCloud(model_descriptor);
        std::vector<int> neigh_indices(1);
        std::vector<float> neigh_square_dists(1);
        //
        kdtree_flann->nearestKSearch(scene_descriptor->front(), 1, neigh_indices, neigh_square_dists);
        const int & best_match = neigh_indices[0];
        float & dist = neigh_square_dists[0];
        //std::cout << "best match: " << best_match << "  " << neigh_square_dists[0] << std::endl;
        if (dist < max_dist) {
            max_dist = dist;
            best_cluster = cloud_cluster;
            //pcl::compute3DCentroid(*best_cluster, it->indices, centroid_scene);
        }
    }
    //visualize(best_cluster, "best_cluster");
    return best_cluster;
}
std::string get_object_name(int index) {
    switch (index) {
        case BIRD:
            return "bird";
        case CAN:
            return "can";
        case CRACKER:
            return "cracker";
        case HOUSE:
            return "house";
        case SHOE:
            return "shoe";
        default:
            return NULL;
    }
}
std::string get_string_to_write(double score, Eigen::Matrix4f transformation) {
    std::string result = "";
    result += "- {score: " + std::to_string(score) + ", R: [";
    result += std::to_string(transformation(0, 0)) + ", " + std::to_string(transformation(0, 1)) + ", " + std::to_string(transformation(0, 2)) + ", ";
    result += std::to_string(transformation(1, 0)) + ", " + std::to_string(transformation(1, 1)) + ", " + std::to_string(transformation(1, 2)) + ", ";
    result += std::to_string(transformation(2, 0)) + ", " + std::to_string(transformation(2, 1)) + ", " + std::to_string(transformation(2, 2)) + "], ";
    result += "t: [" + std::to_string(transformation(0, 3)) + ", " + std::to_string(transformation(1, 3)) + ", " + std::to_string(transformation(2, 3)) + "]}\n";
    return result;
}
//run python script to convert yml to txt - problems loading yaml files
void yaml_to_txt() {
    std::string projectSrcDir = PROJECT_SOURCE_DIR;
    std::string path = projectSrcDir + "/load_gt.py";
    //std::string path = "load_yaml.py";
    std::string py_command = "python";
    py_command = py_command + " " + path;
    system(py_command.c_str());
}
std::vector< Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f >>get_gt_transformation(std::string object_name) {
    std::ifstream infile2("gt_" + object_name + ".txt");
    std::string line2;
    int count2 = 0;
    std::vector< Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f >>  gt_transformations;
    while (std::getline(infile2, line2)) {
        std::istringstream iss(line2);
        
        if (count2 %50 ==0) {
            //break;
            std::istringstream ss2(line2);
            std::string token2;
            double rot_tran[12];
            int q2 = 0;
            while (std::getline(ss2, token2, ',')) {
                rot_tran[q2] = std::stod(token2);
                q2++;
            }
            for (int k = 0; k < 12; k++) {
                std::cout << rot_tran[k] << " ";
            }
            //std::cout << "\n\n\n";
            Eigen::Matrix4f transformation;
            transformation(0, 0) = rot_tran[0];
            transformation(0, 1) = rot_tran[1];
            transformation(0, 2) = rot_tran[2];
            transformation(1, 0) = rot_tran[3];
            transformation(1, 1) = rot_tran[4];
            transformation(1, 2) = rot_tran[5];
            transformation(2, 0) = rot_tran[6];
            transformation(2, 1) = rot_tran[7];
            transformation(2, 2) = rot_tran[8];
            transformation(0, 3) = rot_tran[9];
            transformation(1, 3) = rot_tran[10];
            transformation(2, 3) = rot_tran[11];
            transformation(3, 0) = 0;
            transformation(3, 1) = 0;
            transformation(3, 2) = 0;
            transformation(3, 3) = 1;
            gt_transformations.push_back(transformation);
            //std::cout << "Ground truth Transformation: "<<count2<<"  " << transformation;
        }
        count2++;
    }
    return gt_transformations;
}
int
main(int argc, char** argv) {
    yaml_to_txt();
    for (int n = 1; n <= NUM_CLASSES; n++) {
        //get ground truth transformations for model
        std::vector< Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f >> gt_transformations = get_gt_transformation(get_object_name(n));
        int num_test_images = 0;
        int num_cloud_files = 0;
        std::string object_name = get_object_name(n);
        try {
            path test_object_path(projectSrcDir + "/challenge1_val/test/challenge1_" + std::to_string(n) + "/rgb");
            num_test_images = std::count_if(directory_iterator(test_object_path), directory_iterator(),
                    static_cast<bool(*)(const path&)> (is_regular_file));
            //std::cout << "Count: " << num_test_images << std::endl;
        } catch (const std::exception& e) {
            num_test_images = 0;
            std::cout << "No images of class in test" << n << std::endl;
        }
        if (num_test_images == 0) {
            continue;
        }
        
        //check if .ply files are already present
        if (num_test_images > 0) {
            try {
                path test_object_path(projectSrcDir + "/clouds/" + object_name);
                num_cloud_files = std::count_if(directory_iterator(test_object_path), directory_iterator(),
                        static_cast<bool(*)(const path&)> (is_regular_file));
                //std::cout << "Count files: " << num_cloud_files << std::endl;
            } catch (const std::exception& e) {
                std::cout << "No images of class in clouds" << n << std::endl;
            }
        } else {
            continue;
        }
        //generate .ply files if not present
        if (num_cloud_files == 0) {
            for (int i = 0; i < NUM_TRAIN_IMAGES; i += TRAIN_INC) {
                std::stringstream image_num;
                image_num << setfill('0') << setw(4) << i;
                std::string filename = "/challenge_train/" + object_name + "/rgb/" + image_num.str() + ".png";
                std::cout << "filename: " << filename << std::endl;
                std::string color_filename_model = projectSrcDir + filename;
                //cv::imshow( "colorImage", colorImage);
                std::string depth_filename_model = projectSrcDir + "/challenge_train/" + object_name + "/depth/" + image_num.str() + ".png";
                generate_point_clouds_model(color_filename_model, depth_filename_model, "clouds/" + object_name + "/" + image_num.str() + ".ply");
            }
        }
        for (int scene_itr = 0; scene_itr < num_test_images; scene_itr++) {
            //pcl::PointCloud<PointType>::Ptr final_model_transform(new pcl::PointCloud<PointType>());
            //double best_score = 99999999.0;
            std::clock_t start;
            double duration;
            start = std::clock();
            Eigen::Matrix4f final_transformation;
            std::stringstream image_num;
            image_num << setfill('0') << setw(4) << scene_itr;
            std::string filename = projectSrcDir + "/" + TEAMNAME + "/challenge1_" + std::to_string(n) + "/" + image_num.str() + "_" + get_object_name(n) + ".yml";
            ofstream result_file;
            result_file.open(filename);
            std::string to_file;
            //std::cout << "FILENAME: " << filename << std::endl;
            std::string color_filename_scene = projectSrcDir + "/challenge1_val/test/challenge1_" + std::to_string(n) + "/rgb/" + image_num.str() + ".png";
            std::string depth_filename_scene = projectSrcDir + "/challenge1_val/test/challenge1_" + std::to_string(n) + "/depth/" + image_num.str() + ".png";
            generate_point_clouds_scene(color_filename_scene, depth_filename_scene, "pointcloud_scene.ply");
            pcl::PointCloud<PointType>::Ptr original_scene(new pcl::PointCloud<PointType>());
            pcl::io::loadPLYFile("pointcloud_scene.ply", *original_scene);
            for (int i = 0; i < NUM_TRAIN_IMAGES; i += TRAIN_INC) {
                //Load model and scene
                std::stringstream image_num;
                image_num << setfill('0') << setw(4) << i;
                std::string filename = projectSrcDir + "/clouds/" + object_name + "/" + image_num.str() + ".ply";
                pcl::PointCloud<PointType>::Ptr model(new pcl::PointCloud<PointType>());
                pcl::io::loadPLYFile(filename, *model);
                //keep a copy of original scene
                pcl::PointCloud<PointType>::Ptr scene(new pcl::PointCloud<PointType>);
                copyPointCloud(*original_scene, *scene);
                //visualize(scene, "scene");
                //visualize(model, "model");
                //downsample
                pcl::PointCloud<PointType>::Ptr downsampled_model(new pcl::PointCloud<PointType>());
                pcl::PointCloud<PointType>::Ptr downsampled_scene(new pcl::PointCloud<PointType>());
                model = downsample_cloud(model, downsampling_rad_model, KEYPOINT_MODEL);
                scene = downsample_cloud(scene, downsampling_rad_scene, KEYPOINT_SCENE);
                //std::cout << "Downsampled clouds: " << model->size() << "  " << scene->size() << std::endl;
                //remove planes
                scene = remove_planes(scene);
                scene = euclidean_clustering(model, scene);
                //visualize(scene, "clustered_scene");
                //get keypoints
                pcl::PointCloud<PointType>::Ptr keypoints_model(new pcl::PointCloud<PointType>());
                pcl::PointCloud<PointType>::Ptr keypoints_scene(new pcl::PointCloud<PointType>());
                keypoints_model = generate_keypoints(model, KEYPOINT_VOXELIZATION, KEYPOINT_MODEL);
                keypoints_scene = generate_keypoints(scene, KEYPOINT_VOXELIZATION, KEYPOINT_SCENE);
                //std::cout << "Keypoints size: " << keypoints_model->size() << "  " << keypoints_scene->size() << std::endl;
                //ransac
                //keypoints_scene = eliminate_outliers_ransac(keypoints_scene);
                //get normals
                //visualize_with_keypoints(model, keypoints_model, "model_between_1");
                pcl::PointCloud<pcl::Normal>::Ptr model_normals(new pcl::PointCloud<pcl::Normal>);
                pcl::PointCloud<pcl::Normal>::Ptr scene_normals(new pcl::PointCloud<pcl::Normal>);
                model_normals = get_normals(model);
                scene_normals = get_normals(scene);
                //std::cout << "Normals calculated: " << model_normals->size() << "  " << scene_normals->size() << std::endl;
                //visualize_with_keypoints(model, keypoints_model, "model_between_2");
                //Descriptor
                //std::cout << "Computing descriptors" << std::endl;
                pcl::PointCloud<pcl::SHOT352>::Ptr descriptors_model(new pcl::PointCloud<pcl::SHOT352>());
                pcl::PointCloud<pcl::SHOT352>::Ptr descriptors_scene(new pcl::PointCloud<pcl::SHOT352>());
                pcl::SHOTEstimationOMP<PointType, pcl::Normal, pcl::SHOT352> shot_estimation;
                shot_estimation.setNumberOfThreads(4);
                shot_estimation.setRadiusSearch(shot_support_rad);
                shot_estimation.setInputCloud(keypoints_model);
                shot_estimation.setInputNormals(model_normals);
                shot_estimation.setSearchSurface(model);
                shot_estimation.compute(*descriptors_model);
                shot_estimation.setInputCloud(keypoints_scene);
                shot_estimation.setInputNormals(scene_normals);
                shot_estimation.setSearchSurface(scene);
                shot_estimation.compute(*descriptors_scene);
                //std::cout << "Computed descriptors: " << descriptors_model->size() << "  " << descriptors_scene->size() << std::endl;
                //Find correspondences
                //std::cout << "Finding Correspondences" << std::endl;
                pcl::CorrespondencesPtr correspondences(new pcl::Correspondences());
                pcl::KdTreeFLANN<DescriptorType> kdtree_flann;
                kdtree_flann.setInputCloud(descriptors_model);
                for (size_t i = 0; i < descriptors_scene->size(); ++i) {
                    if (!pcl_isfinite(descriptors_scene->at(i).descriptor[0])) {
                        continue;
                    }
                    std::vector<int> neigh_indices(1);
                    std::vector<float> neigh_square_dists(1);
                    int found_neighs = kdtree_flann.nearestKSearch(descriptors_scene->at(i), 1, neigh_indices, neigh_square_dists);
                    if (found_neighs == 1 && neigh_square_dists[0] < 0.25f) {
                        pcl::Correspondence correspondence(neigh_indices[0], static_cast<int> (i), neigh_square_dists[0]);
                        correspondences->push_back(correspondence);
                        //std::cout << correspondence << std::endl;
                    }
                }
                //std::cout << "Found correspondences: " << correspondences->size() << std::endl;
                //std::cout << "Clustering..." << std::endl;
                std::vector<pcl::Correspondences> clusters; //output
                pcl::GeometricConsistencyGrouping<PointType, PointType> gc_clusterer;
                float cg_size = 0.2; //0.02
                float cg_thres = 8; //5
                gc_clusterer.setGCSize(cg_size); //1st param
                gc_clusterer.setGCThreshold(cg_thres); //2nd param
                gc_clusterer.setInputCloud(keypoints_model);
                gc_clusterer.setSceneCloud(keypoints_scene);
                gc_clusterer.setModelSceneCorrespondences(correspondences);
                std::vector< Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f > > transformations;
                gc_clusterer.recognize(transformations, clusters);
                //std::cout << "Number of clusters: " << transformations.size() << "  " << clusters.size() << std::endl;
                int num_hypothesis = transformations.size();
                //std::cout << "Number of hypothesis is " << num_hypothesis << std::endl;
                std::vector<pcl::PointCloud<PointType>::ConstPtr> registered_clouds;
                pcl::PointCloud<PointType>::Ptr cloud_trg = scene;
                pcl::PointCloud<PointType>::Ptr clouds_ref(new pcl::PointCloud<PointType>());
                double score_arr[num_hypothesis];
                std::vector< Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f > > icp_transformations;
                for (int i = 0; i < num_hypothesis; i++) {
                    //std::cout << "transform: " << transformations[i] << std::endl;
                    pcl::transformPointCloud(*model, *clouds_ref, transformations[i]);
                    pcl::IterativeClosestPoint<PointType, PointType> icp;
                    //icp.setMaxCorrespondenceDistance(0.1);
                    icp.setMaximumIterations(icp.getMaximumIterations()*3);
                    //icp.setUseReciprocalCorrespondences(true);
                    icp.setInputSource(clouds_ref);
                    icp.setInputTarget(cloud_trg);
                    pcl::PointCloud<PointType>::Ptr registered(new pcl::PointCloud<PointType>);
                    icp.align(*registered);
                    registered_clouds.push_back(registered);
                    Eigen::Matrix4f transformation = icp.getFinalTransformation();
                    score_arr[i] = icp.getFitnessScore(10);
                    icp_transformations.push_back(transformation);
                }
                //Verify hypothesis
                std::vector<bool> mask_hv;
                mask_hv = greedy_verify_hypothesis(registered_clouds, scene);
                //visualize
                pcl::PointCloud<PointType>::Ptr best_hyp(new pcl::PointCloud<PointType>());
                for (int i = 0; i < mask_hv.size(); i++) {
                    if (mask_hv[i]) {
                        //std::cout << "MASKKK******************";
                        *best_hyp = *registered_clouds[i];
                        //std::cout << "SCORE: " << score_arr[i];
                        //Final transformation to be written to file. Shouldn't we inverse ground truth??
                        Eigen::Matrix4f result_transformations = icp_transformations[i] * transformations[i];
                        result_transformations = result_transformations * gt_transformations[i/TRAIN_INC];
                        //result_transformations = result_transformations.inverse().eval();
                        to_file += get_string_to_write(score_arr[i], result_transformations);
                        for (int j = 0; j < (*best_hyp).points.size(); j++) {
                            (*best_hyp).points[j].r = 0;
                            (*best_hyp).points[j].b = 0;
                            (*best_hyp).points[j].g = 255;
                        }
                        visualize_model_scene(best_hyp, original_scene, "Final Result");
                    }
                }
            }
            duration = (std::clock() - start) / (double) CLOCKS_PER_SEC;
            result_file << "run_time: " << duration << "\n";
            result_file << "ests:\n";
            result_file << to_file;
            result_file.close();
        }
    }
    return 0;
}

