#include <iostream>
#include <opencv2/opencv.hpp>
int main(int argc, char* argv[])
{
    // File path for input image
    std::string projectSrcDir = PROJECT_SOURCE_DIR;
    std::string inputImagePath = projectSrcDir + "/Data/serval.png";
    
    //// Task 1: Basisc //////////////////////////////////////////////////
    // Step 1: Load image using cv::imread
	cv::Mat img = cv::imread(inputImagePath, CV_LOAD_IMAGE_ANYCOLOR );
	if (img.empty())
		return -1;
	cv::namedWindow("image", cv::WINDOW_AUTOSIZE);
	
    
    // Step 2: Display image using cv::imshow
	cv::imshow("image", img);
	cv::waitKey(4000);
	//cv::destroyWindow("image");
	
    // Step 3: Convert to gray image using cv::cvtColor function
	cv::Mat im_gray;
	cv::cvtColor(img, im_gray, CV_RGB2GRAY);
	cv::imshow("gray", im_gray);
	cv::waitKey(4000);
	
    // Step 4: Save gray image by using cv::imwrite
	cv::imwrite("img_gray.jpg" , im_gray );
	//cv::destroyWindow("gray");
    
    
    //// Task 2-1: Contrast stretching //////////////////////////////////
    // Step 1: Apply contrast streching operator for a gray image
	double min, max;
	cv::minMaxLoc(im_gray , &min , &max);
	im_gray = (255 * (im_gray - min)) / (max - min);
    // Step 2: Show the result
	cv::imshow("contrast", im_gray);
	cv::waitKey(4000);
    
    //// Task 2-2: Color inverting /////////////////////////////////////
    // Step 1: Apply invert operator for a color image
	cv::Mat img_new = cv::Scalar::all(255) - img;
	cv::Mat img2;
	cv::subtract(cv::Scalar::all(255), img, img2);
    
    // Step 2: Show the result
	cv::imshow("inversion", img_new);
	cv::waitKey(4000);
	cv::imshow("inversion new", img2);
	cv::waitKey(4000);
    
    //// Task 3: Local operaters ///////////////////////////////////////
    // Step 1: Apply mean filter and show the reuslt
	cv::Mat mean;
	//int kernel_size = 3;
	//cv::Mat kernel = cv::Mat::ones(kernel_size , kernel_size, CV_32F)/(float)(kernel_size*kernel_size);
	cv::boxFilter(im_gray, mean , -1 , cv::Size(13,13));
	cv::imshow("mean filter", mean);
	cv::waitKey(4000);
	
    
    // Step 2: Apply Gaussian filter and show the reuslt
	cv::Mat gauss;
	cv::GaussianBlur(im_gray, gauss, cv::Size(13, 13), 0);
	cv::imshow("gaussian filter", gauss);
	cv::waitKey(4000);
	
    // Step 3: Apply Bilateral filter and show the reuslt
	float kernel_length;
	kernel_length = 31;
	cv::Mat bilateral;
	cv::bilateralFilter(im_gray, bilateral, kernel_length , kernel_length*2, kernel_length/2);
	cv::imshow("bilateral filter", bilateral);
	cv::waitKey(4000);
    
    //// Task 4: Image Subtraction /////////////////////////////////////
    // Step 1: load two image
    std::string inputImagePathA = projectSrcDir + "/Data/imgA.png";
    std::string inputImagePathB = projectSrcDir + "/Data/imgB.png";
	cv::Mat imgA = cv::imread(inputImagePathA, CV_LOAD_IMAGE_ANYCOLOR);
	if (imgA.empty())
		return -1;
	cv::namedWindow("imageA", cv::WINDOW_AUTOSIZE);
	cv::imshow("imageA", imgA);
	cv::Mat imgB = cv::imread(inputImagePathB, CV_LOAD_IMAGE_ANYCOLOR);
	if (imgB.empty())
		return -1;
	cv::namedWindow("imageB", cv::WINDOW_AUTOSIZE);
	cv::imshow("imageB", imgB);
    // Step 2: Subtract images and save the result
	cv::Mat imgC = imgB - imgA;
	cv::imshow("image diff", imgC);
	cv::waitKey(4000);
	cv::destroyAllWindows();
    return 0;
}

