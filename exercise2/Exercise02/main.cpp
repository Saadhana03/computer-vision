
#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/features2d.hpp"
using namespace cv;
using namespace std;
//Takes a descriptor and turns it into an xy point
void keypoints2points(const vector<KeyPoint>& in, vector<Point2f>& out)
{
	out.clear();
	out.reserve(in.size());
	for (size_t i = 0; i < in.size(); ++i)
	{
		out.push_back(in[i].pt);
	}
}
/**
* @brief Draw rectangle of estimated homography in the scene image
* @param [in]    imgObjSize : image size of object image
* @param [in]    homography : homography matrix (3x3)
* @param [inout] imgMatches : scene image
*/
void drawHomographyRect(cv::Size imgObjSize, cv::Mat& homography, cv::Mat& imgMatches)
{
	if (homography.empty() || imgMatches.cols == 0) {
		return;
	}
	// define object corners in the object image
	std::vector<cv::Point2d> cornersObj(4);
	cornersObj[0] = cv::Point2d(0, 0);
	cornersObj[1] = cv::Point2d(imgObjSize.width, 0);
	cornersObj[2] = cv::Point2d(imgObjSize.width, imgObjSize.height);
	cornersObj[3] = cv::Point2d(0, imgObjSize.height);
	// compute object corners in the scene image using homography
	std::vector<cv::Point2d> cornersScene(4);
	perspectiveTransform(cornersObj, cornersScene, homography);
	// draw rectangle of detected object
	cv::line(imgMatches, cornersScene[0] + cv::Point2d(imgObjSize.width, 0), cornersScene[1] + cv::Point2d(imgObjSize.width, 0), cv::Scalar(255, 255, 0), 5);
	cv::line(imgMatches, cornersScene[1] + cv::Point2d(imgObjSize.width, 0), cornersScene[2] + cv::Point2d(imgObjSize.width, 0), cv::Scalar(255, 255, 0), 5);
	cv::line(imgMatches, cornersScene[2] + cv::Point2d(imgObjSize.width, 0), cornersScene[3] + cv::Point2d(imgObjSize.width, 0), cv::Scalar(255, 255, 0), 5);
	cv::line(imgMatches, cornersScene[3] + cv::Point2d(imgObjSize.width, 0), cornersScene[0] + cv::Point2d(imgObjSize.width, 0), cv::Scalar(255, 255, 0), 5);
	cv::namedWindow("Draw rectangle");
	cv::imshow("Draw rectangle", imgMatches);
	cv::waitKey(0);
}
int main(int argc, char* argv[])
{
	std::string projectSrcDir = PROJECT_SOURCE_DIR;
	// Load images
	//Please also select different RANSAC threshold parameter as mentioned on line 170
	//cv::Mat imgObj = cv::imread(projectSrcDir + "/Data/chocolate1.png");
	//cv::Mat imgObj = cv::imread(projectSrcDir + "/Data/chocolate4.jpg");
	//cv::Mat imgObj = cv::imread(projectSrcDir + "/Data/chocolate3.jpg");
	cv::Mat imgObj = cv::imread(projectSrcDir + "/Data/chocolate2.png");
	cv::Mat imgScene = cv::imread(projectSrcDir + "/Data/chocolate_scene.png");
	// Step a)1: Detect keypoints and extract descriptors
	//ORB Detector
	// Initiate ORB detector
	std::vector<cv::KeyPoint> keypoints_obj;
	std::vector<cv::KeyPoint> keypoints_scene;
	//std::vector<cv::DescriptorExtractor> descriptors_obj;
	//std::vector<cv::DescriptorExtractor> descriptors_scene;
	cv::Ptr<cv::FeatureDetector> detector = cv::ORB::create();
	// find the keypoints and descriptors with ORB
	detector->detect(imgObj, keypoints_obj);
	detector->detect(imgScene, keypoints_scene);
	// Step a)2: Draw keypoints
	cv::Mat obj_out;
	cv::drawKeypoints(imgObj, keypoints_obj, obj_out);
	cv::imshow("Object keypoints", obj_out);
	cv::waitKey(4000);
	cv::Mat scene_out;
	cv::drawKeypoints(imgScene, keypoints_scene, scene_out);
	cv::imshow("Scene keypoints", scene_out);
	cv::waitKey(4000);
	// Step b)1: Match descriptors
	cv::Mat descriptors_obj, descriptors_scene;
	cv::Ptr<cv::DescriptorExtractor> extractor = cv::ORB::create();
	extractor->compute(imgObj, keypoints_obj, descriptors_obj);
	extractor->compute(imgScene, keypoints_scene, descriptors_scene);
	cv::BFMatcher matcher(cv::NORM_HAMMING, true);
	//cv::BFMatcher matcher(cv::NORM_HAMMING2);
	//std::vector< std::vector< cv::DMatch > >matches;
	std::vector< cv::DMatch > matches;
	matcher.match(descriptors_obj, descriptors_scene, matches);
	// Step b)2: Display corresponding pair
	cv::Mat img_matches;
	cv::drawMatches(imgObj, keypoints_obj, imgScene, keypoints_scene, matches, img_matches);
	//-- Show detected matches
	cv::imshow("Matches", img_matches);
	cv::imwrite("img_matches.jpg", img_matches);
	cv::waitKey(1000);
	// Step c)1: Compute homography with RANSAC
	std::vector<cv::Point2f> object_b, scene_b;
	//keypoints2points(keypoints_obj, object_b);
	//keypoints2points(keypoints_scene, scene_b);
	//cv::KeyPoint::convert(keypoints_obj, object_b );
	//cv::KeyPoint::convert(keypoints_scene, scene_b);
	//Copt keypoint locations
	//Find good matches
	cv::Mat img_matches_new;
	std::vector<std::vector<cv::DMatch>> matches_new;
	cv::BFMatcher matcher1;
	matcher1.knnMatch(descriptors_obj, descriptors_scene, matches_new, 2);  // Find two nearest matches
	//drawMatches(imgObj, keypoints_obj, imgScene, keypoints_scene, matches_new, img_matches_new);
	//imshow("Matches new", img_matches_new);
	//cv::waitKey(1000);
	vector<cv::DMatch> good_matches;
	for (int i = 0; i < matches_new.size(); ++i)
	{
		const float ratio = 0.95; // As in Lowe's paper; can be tuned
		if (matches_new[i][0].distance < ratio * matches_new[i][1].distance)
		{
			good_matches.push_back(matches_new[i][0]);
		}
	}
	cv::drawMatches(imgObj, keypoints_obj, imgScene, keypoints_scene, good_matches, img_matches_new);
	cv::imshow("Matches new", img_matches_new);
	cv::waitKey(1000);
	for (int i = 0; i < good_matches.size(); i++)
	{
		object_b.push_back(keypoints_obj[matches[i].queryIdx].pt);
		scene_b.push_back(keypoints_scene[matches[i].trainIdx].pt);
	}
	cv::Mat masks;
	vector<KeyPoint> inliers1, inliers2;
	vector<DMatch> inlier_matches;
	cv::Mat homography;
	if (matches.size() >= 4)
	{
		try
		{
			//For chocolate5 use this one
			//homography = cv::findHomography(object_b, scene_b, masks, cv::RANSAC, 1);
			//for chocolate4 use this one
			//homography = cv::findHomography(object_b, scene_b, masks, cv::RANSAC, 5);
			//For chocolate2 use this one
			homography = cv::findHomography(object_b, scene_b, masks, cv::RANSAC, 5);
			//imshow("Homography", homography);
			//cv::waitKey(0);
		}
		catch (cv::Exception & e)
		{
			std::cerr << "Priya" << e.msg << std::endl;
		}
	}
	else
		std::cout << "Not enough matches found";
	/*
	// Step c)2: Display inlier matches
	int n=masks.rows;
	std::vector<cv::KeyPoint> inliersObj;
	std::vector<cv::KeyPoint> inliersScene;
	std::vector<cv::DMatch> inlierMatches;
	for(int i=0;i<n;i++)
	{
	if((unsigned int)masks.at<int>(i,0)) {
	inliersObj.push_back(keypointsObj.at(matches[i].queryIdx));
	inliersScene.push_back(keypointsScene.at(matches[i].trainIdx));
	cv::DMatch dMatch=*(new cv::DMatch(i,i, matches[i].imgIdx, matches[i].distance));
	inlierMatches.push_back(dMatch);
	}
	}
	cv::Mat inlierMatchedImage;
	try {
	cv::drawMatches(imgObj, inliersObj, imgScene, inliersScene, inlierMatches, inlierMatchedImage);
	} catch(cv::Exception &e) {
	cv::imshow("window1", inlierMatchedImage);
	cv::waitKey(0);
	std::cout<<e.msg<<std::endl;
	}
	*/
	for (unsigned i = 0; i < good_matches.size(); i++) {
		if (masks.at<uchar>(i)) {
			int new_i = static_cast<int>(inliers1.size());
			inliers1.push_back(keypoints_obj[i]);
			inliers2.push_back(keypoints_scene[i]);
			inlier_matches.push_back(DMatch(new_i, new_i, 0));
		}
	}
	int inliers = (int)inliers1.size();
	std::cout << "Number of inliers are: " << inliers;
	float ratio = inliers * 1.0 / (matches.size());
	cv::Mat res;
	cv::drawMatches(imgObj, inliers1, imgScene, inliers2, inlier_matches, res);
	//imshow("Inlier", res);
	//cv::imwrite("img_matches.jpg", res);
	cv::waitKey(1000);
	// Step c)2: Display inlier matches
	cv::imshow("Inlier Detection", res);
	cv::waitKey(1000);
	// Step c)3: Display object rectangle in the scene using drawHomographyRect()
	cv::Size size = cv::Size(imgObj.rows, imgObj.cols); //Size type
	drawHomographyRect(size, homography, res);
	// Step d: Overlay another object on the detected object in the scene for augmented reality
	//cv::Mat arObj = cv::imread(projectSrcDir + "/Data/chocolate2.png");  //Image to be overlayed
	cv::Mat arObj = imgObj;
	cv::Mat imgMask = cv::Mat(arObj.size(), CV_8UC1, cv::Scalar(255));  //Temp mask of object size
	cv::Mat imgMaskWarped;
	cv::Mat revImgMaskWarped;
	cv::warpPerspective(imgMask, imgMaskWarped, homography, imgScene.size());  //Mask to copy just the object
	revImgMaskWarped = 255 - imgMaskWarped;  //Mask to copy just the background
	cv::Mat imgTrainWarped;
	cv::warpPerspective(arObj, imgTrainWarped, homography, imgScene.size());  //Warp object in a black image
	cv::Mat tem1;
	cv::Mat tem2;
	imgTrainWarped.copyTo(tem1, imgMaskWarped); //Copy only object
	imgScene.copyTo(tem2, revImgMaskWarped);  //Copy only the background
	cv::Mat newImg = tem1 + tem2;
	cv::imshow("Overlay Image", newImg);
	cv::waitKey(0);
	// Step e: Implement live demo for the object detector, using web-camera
	cv::VideoCapture cap;
	cap.open(0);
	arObj = cv::imread(projectSrcDir + "/Data/chocolate2.png");
	while (true)
	{
		cap >> imgScene;
		// Detect planer object, using the code from step a) to d).
		// show the detection result and overlay AR image
		//cv::imshow( "scene image", imgScene);
		//Detect keypoints and get corresponding descriptors
		cv::Ptr<cv::ORB> detectorVid = cv::ORB::create();
		detectorVid->detectAndCompute(arObj, cv::Mat(), keypoints_obj, descriptors_obj);
		detectorVid->detectAndCompute(imgScene, cv::Mat(), keypoints_scene, descriptors_scene);
		if (descriptors_obj.type() != CV_32F) {
			descriptors_obj.convertTo(descriptors_obj, CV_32F);
		}
		if (descriptors_scene.type() != CV_32F) {
			descriptors_scene.convertTo(descriptors_scene, CV_32F);
		}
		cv::FlannBasedMatcher matcher2;
		std::vector< cv::DMatch > matches2;
		//matcher.match(descriptors_obj, descriptors_scene, matches2);
		//Matching
		try {
			matcher2.match(descriptors_obj, descriptors_scene, matches2);
		}
		catch (cv::Exception & e)
		{
			std::cerr << "Priya" << e.msg << std::endl;
		}
		cv::drawMatches(arObj, keypoints_obj, imgScene, keypoints_scene, matches2, img_matches);
		cv::imshow("newImg", img_matches);
		std::vector<cv::Point2f> objVid;
		std::vector<cv::Point2f> sceneVid;
		for (int i = 0; i < matches2.size(); i++)
		{
			objVid.push_back(keypoints_obj[matches2[i].queryIdx].pt);
			sceneVid.push_back(keypoints_scene[matches2[i].trainIdx].pt);
		}
		homography = cv::findHomography(objVid, sceneVid, masks,
			cv::RANSAC, 3);
		cv::warpPerspective(imgMask, imgMaskWarped, homography, imgScene.size());
		revImgMaskWarped = 255 - imgMaskWarped;
		cv::warpPerspective(arObj, imgTrainWarped, homography, imgScene.size());
		cv::Mat tem1Vid;
		cv::Mat tem2Vid;
		imgTrainWarped.copyTo(tem1Vid, imgMaskWarped);
		imgScene.copyTo(tem2Vid, revImgMaskWarped);
		newImg = tem1Vid + tem2Vid;
		cv::imshow("newImg", newImg);
		int key = cv::waitKey(1);
		if (key == 'q' || key == 27) {
			break;
		}
	}
	return 0;
}
