#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/correspondence.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/board.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/recognition/cg/hough_3d.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <pcl/recognition/hv/hv_go.h>
#include <pcl/registration/icp.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/recognition/hv/greedy_verification.h>
#include <pcl/io/ply_io.h>
typedef pcl::PointXYZRGBA PointType;
typedef pcl::Normal NormalType;
typedef pcl::ReferenceFrame RFType;
typedef pcl::SHOT352 DescriptorType;
int
main (int argc, char** argv)
{
	pcl::PointCloud<PointType>::Ptr model_keypoints(new pcl::PointCloud<PointType>());
	pcl::PointCloud<PointType>::Ptr scene_keypoints(new pcl::PointCloud<PointType>());
	pcl::PointCloud<NormalType>::Ptr model_normals(new pcl::PointCloud<NormalType>());
	pcl::PointCloud<NormalType>::Ptr scene_normals(new pcl::PointCloud<NormalType>());
	pcl::PointCloud<DescriptorType>::Ptr model_descriptors(new pcl::PointCloud<DescriptorType>());
	pcl::PointCloud<DescriptorType>::Ptr scene_descriptors(new pcl::PointCloud<DescriptorType>());
    std::string projectSrcDir = PROJECT_SOURCE_DIR;
    
    //// a) Load Point clouds (model and scene)
    pcl::PointCloud<PointType>::Ptr model (new pcl::PointCloud<PointType> ());
    std::string model_filename_ = projectSrcDir + "/Data/model_house.pcd";
    
    pcl::PointCloud<PointType>::Ptr scene (new pcl::PointCloud<PointType> ());
    std::string scene_filename_ = projectSrcDir + "/Data/scene_clutter.pcd";
    
	// Load model_house.pcd
	if (pcl::io::loadPCDFile<PointType>(model_filename_, *model) == -1) //* load the file
	{
		PCL_ERROR("Couldn't read file model_house.pcd \n");
		return (-1);
	}
	std::cout << "Loaded "
		<< model->width * model->height
		<< " data points from model_house.pcd with the following fields: "
		<< std::endl;
	// Load scene_clutter.pcd
	if (pcl::io::loadPCDFile<PointType>(scene_filename_, *scene) == -1) //* load the file
	{
		PCL_ERROR("Couldn't read file scene_clutter.pcd \n");
		return (-1);
	}
	std::cout << "Loaded "
		<< scene->width * scene->height
		<< " data points from scene_clutter.pcd with the following fields: "
		<< std::endl;
    
    //// a) Compute normals
	pcl::NormalEstimationOMP<PointType, NormalType> norm_est;
	norm_est.setNumberOfThreads(2);
	norm_est.setKSearch(10);
	norm_est.setInputCloud(model);
	norm_est.compute(*model_normals);
	norm_est.setInputCloud(scene);
	norm_est.compute(*scene_normals);
    
    //// b) Extract key-points from point clouds by downsampling point clouds
	//UNIFORM SAMPLING
	pcl::UniformSampling<PointType> uniform_sampling;
	uniform_sampling.setInputCloud (model);
	uniform_sampling.setRadiusSearch (0.01f); //0.02f
	uniform_sampling.filter (*model_keypoints);
	std::cout << "Model total points: " << model->size () << "; Selected Keypoints: " << model_keypoints->size () << std::endl;
	uniform_sampling.setInputCloud (scene);
	uniform_sampling.setRadiusSearch (0.04f); //0.02f
	uniform_sampling.filter (*scene_keypoints);
	std::cout << "Scene total points: " << scene->size () << "; Selected Keypoints: " << scene_keypoints->size () << std::endl;
    
    //// c) Compute descriptor for keypoints
	pcl::SHOTEstimationOMP<PointType, NormalType, DescriptorType> descr_est;
	descr_est.setNumberOfThreads(2);
	descr_est.setRadiusSearch(1.5f); //1.0f
	descr_est.setInputCloud(model_keypoints);
	descr_est.setInputNormals(model_normals);
	descr_est.setSearchSurface(model);
	descr_est.compute(*model_descriptors);
	descr_est.setInputCloud(scene_keypoints);
	descr_est.setInputNormals(scene_normals);
	descr_est.setSearchSurface(scene);
	descr_est.compute(*scene_descriptors);
    
    //// d) Find model-scene key-points correspondences with KdTree
	pcl::CorrespondencesPtr model_scene_corrs(new pcl::Correspondences());
	pcl::KdTreeFLANN<DescriptorType> match_search;
	match_search.setInputCloud(model_descriptors);
	std::vector<int> model_good_keypoints_indices;
	std::vector<int> scene_good_keypoints_indices;
	//  For each scene keypoint descriptor, find nearest neighbor into the model keypoints descriptor cloud and add it to the correspondences vector.
	for (size_t i = 0; i < scene_descriptors->size(); ++i)
	{
		std::vector<int> neigh_indices(1);
		std::vector<float> neigh_sqr_dists(1);
		if (!pcl_isfinite(scene_descriptors->at(i).descriptor[0]))  //skipping NaNs
		{
			continue;
		}
		int found_neighs = match_search.nearestKSearch(scene_descriptors->at(i), 1, neigh_indices, neigh_sqr_dists);
		if (found_neighs == 1 && neigh_sqr_dists[0] < 0.9f)
		{
			pcl::Correspondence corr(neigh_indices[0], static_cast<int> (i), neigh_sqr_dists[0]);
			model_scene_corrs->push_back(corr);
			model_good_keypoints_indices.push_back(corr.index_query);
			scene_good_keypoints_indices.push_back(corr.index_match);
		}
	}
	pcl::PointCloud<PointType>::Ptr model_good_kp(new pcl::PointCloud<PointType>());
	pcl::PointCloud<PointType>::Ptr scene_good_kp(new pcl::PointCloud<PointType>());
	pcl::copyPointCloud(*model_keypoints, model_good_keypoints_indices, *model_good_kp);
	pcl::copyPointCloud(*scene_keypoints, scene_good_keypoints_indices, *scene_good_kp);
	std::cout << "Correspondences found: " << model_scene_corrs->size() << std::endl;
	std::cout << "DOne" << std::endl;
    
    //// e) Cluster geometrical correspondence, and finding object instances
	std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > rototranslations;
	std::vector<pcl::Correspondences> clustered_corrs;
	pcl::GeometricConsistencyGrouping<PointType, PointType> gc_clusterer;
	gc_clusterer.setGCSize(0.005f);
	gc_clusterer.setGCThreshold(2.0f);
	gc_clusterer.setInputCloud(model_keypoints);
	gc_clusterer.setSceneCloud(scene_keypoints);
	gc_clusterer.setModelSceneCorrespondences(model_scene_corrs);
	//gc_clusterer.cluster (clustered_corrs);
	gc_clusterer.recognize(rototranslations, clustered_corrs);
	/**
	* Stop if no instances
	*/
	if (rototranslations.size() <= 0)
	{
		cout << "*** No instances found! ***" << endl;
		return (0);
	}
	else
	{
		cout << "Recognized Instances: " << rototranslations.size() << endl << endl;
	}
    
    //// f) Refine pose of each instance by using ICP
	std::vector<pcl::PointCloud<PointType>::ConstPtr> instances;
	for (size_t i = 0; i < rototranslations.size(); ++i)
	{
		pcl::PointCloud<PointType>::Ptr rotated_model(new pcl::PointCloud<PointType>());
		pcl::transformPointCloud(*model, *rotated_model, rototranslations[i]);
		instances.push_back(rotated_model);
	}
	
	std::cout << "hyothesis: " << instances.size();
	
	//std::vector<pcl::PointCloud<PointType>::ConstPtr> registeredHypothesis;
	
	std::vector<pcl::PointCloud<PointType>::ConstPtr> registered_instances;
	if (true)
	{
		cout << "--- ICP ---------" << endl;
		for (size_t i = 0; i < rototranslations.size(); ++i)
		{
			pcl::IterativeClosestPoint<PointType, PointType> icp;
			icp.setMaximumIterations(5);
			icp.setMaxCorrespondenceDistance(0.005f);
			icp.setInputTarget(scene);
			icp.setInputSource(instances[i]);
			pcl::PointCloud<PointType>::Ptr registered(new pcl::PointCloud<PointType>);
			icp.align(*registered);
			registered_instances.push_back(registered);
			cout << "Instance " << i << " ";
			if (icp.hasConverged())
			{
				cout << "Aligned!" << endl;
			}
			else
			{
				cout << "Not Aligned!" << endl;
			}
		}
		cout << "-----------------" << endl << endl;
	}
	
    
    //// g) Do hypothesis verification
	float lambda = 3.f;
	pcl::GreedyVerification<PointType, PointType> greedy_hv(lambda);
	greedy_hv.setResolution(0.005f); //voxel grid is applied beforehand
	greedy_hv.setInlierThreshold(0.005f);
	greedy_hv.setSceneCloud(scene);
	greedy_hv.addModels(registered_instances, true);
	greedy_hv.verify();
	std::vector<bool> mask_hv;
	greedy_hv.getMask(mask_hv);
    
	if (mask_hv.size() != registered_instances.size()) {
		std::cout << "GreedyVerification wrong vector size";
	}
	//hyposthesis verification
	std::vector<pcl::PointCloud<PointType>::ConstPtr> verified_hypotheses;
	for (int i = 0; i < mask_hv.size(); i++) {
		if (mask_hv[i]) {
			verified_hypotheses.push_back(registered_instances[i]);
			std::cout << "GreedyVerification: Hypothese " << i << " is CORRECT";
		}
		else {
			std::cout << "GreedyVerification: Hypothese " << i << " is NOT correct";
		}
	}
    /// Visualize detection result
    
	std::cout << "Model instances found: " << rototranslations.size() << std::endl;
	for (size_t i = 0; i < rototranslations.size(); ++i)
	{
		std::cout << "\n    Instance " << i + 1 << ":" << std::endl;
		std::cout << "        Correspondences belonging to this instance: " << clustered_corrs[i].size() << std::endl;
		// Print the rotation matrix and translation vector
		Eigen::Matrix3f rotation = rototranslations[i].block<3, 3>(0, 0);
		Eigen::Vector3f translation = rototranslations[i].block<3, 1>(0, 3);
		printf("\n");
		printf("            | %6.3f %6.3f %6.3f | \n", rotation(0, 0), rotation(0, 1), rotation(0, 2));
		printf("        R = | %6.3f %6.3f %6.3f | \n", rotation(1, 0), rotation(1, 1), rotation(1, 2));
		printf("            | %6.3f %6.3f %6.3f | \n", rotation(2, 0), rotation(2, 1), rotation(2, 2));
		printf("\n");
		printf("        t = < %0.3f, %0.3f, %0.3f >\n", translation(0), translation(1), translation(2));
	}
	//---------------------------------------------
	pcl::visualization::PCLVisualizer viewer("Correspondence Grouping");
	viewer.addPointCloud(scene, "scene_cloud");
	pcl::PointCloud<PointType>::Ptr off_scene_model(new pcl::PointCloud<PointType>());
	pcl::PointCloud<PointType>::Ptr off_scene_model_keypoints(new pcl::PointCloud<PointType>());
	bool show_keypoints_(true);
	bool show_correspondences_(true);
	if (show_correspondences_ || show_keypoints_)
	{
		//  We are translating the model so that it doesn't end in the middle of the scene representation
		pcl::transformPointCloud(*model, *off_scene_model, Eigen::Vector3f(-1, 0, 0), Eigen::Quaternionf(1, 0, 0, 0));
		pcl::transformPointCloud(*model_keypoints, *off_scene_model_keypoints, Eigen::Vector3f(-1, 0, 0), Eigen::Quaternionf(1, 0, 0, 0));
		pcl::visualization::PointCloudColorHandlerCustom<PointType> off_scene_model_color_handler(off_scene_model, 255, 255, 128);
		viewer.addPointCloud(off_scene_model, off_scene_model_color_handler, "off_scene_model");
	}
	if (show_keypoints_)
	{
		pcl::visualization::PointCloudColorHandlerCustom<PointType> scene_keypoints_color_handler(scene_keypoints, 0, 0, 255);
		viewer.addPointCloud(scene_keypoints, scene_keypoints_color_handler, "scene_keypoints");
		viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "scene_keypoints");
		pcl::visualization::PointCloudColorHandlerCustom<PointType> off_scene_model_keypoints_color_handler(off_scene_model_keypoints, 0, 0, 255);
		viewer.addPointCloud(off_scene_model_keypoints, off_scene_model_keypoints_color_handler, "off_scene_model_keypoints");
		viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "off_scene_model_keypoints");
	}
	for (size_t i = 0; i < rototranslations.size(); ++i)
	{
		pcl::PointCloud<PointType>::Ptr rotated_model(new pcl::PointCloud<PointType>());
		pcl::transformPointCloud(*model, *rotated_model, rototranslations[i]);
		std::stringstream ss_cloud;
		ss_cloud << "instance" << i;
		pcl::visualization::PointCloudColorHandlerCustom<PointType> rotated_model_color_handler(rotated_model, 255, 0, 0);
		viewer.addPointCloud(rotated_model, rotated_model_color_handler, ss_cloud.str());
		if (show_correspondences_)
		{
			for (size_t j = 0; j < clustered_corrs[i].size(); ++j)
			{
				std::stringstream ss_line;
				ss_line << "correspondence_line" << i << "_" << j;
				PointType& model_point = off_scene_model_keypoints->at(clustered_corrs[i][j].index_query);
				PointType& scene_point = scene_keypoints->at(clustered_corrs[i][j].index_match);
				//  We are drawing a line for each pair of clustered correspondences found between the model and the scene
				viewer.addLine<PointType, PointType>(model_point, scene_point, 0, 255, 0, ss_line.str());
			}
		}
	}
	while (!viewer.wasStopped())
	{
		viewer.spinOnce();
	}
    
    return 0;
}
